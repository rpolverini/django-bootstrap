import random

from django.shortcuts import render


def home(request):
    """View function for home page of site."""
    numero = random.random()
    print(numero)
    ctx = {'numero': numero}
    return render(request, 'home.html', ctx)
